<?php

Route::namespace('Web')->group(function () {
    // Controllers Within The "App\Http\Controllers\Web" Namespace
    Route::get('/', 'PageController@index')->name('home');
    Route::get('/search', 'PageController@search')->name('search');

});
