<?php

namespace App\Http\Controllers\Web;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    //
    public function index()
    {
        return view('Front.home');
    }

    public function search(Request $request)
    {
        $item = $request->item;
        $client = new Client(
            ['headers' =>
                ['content-type' => 'application/json']
            ]
        );
        $response = $client->request('get', 'http://localhost:9200/_search/?q=' . $item);
        $response = $response->getBody();
        $response = json_decode($response);
        $response = $response->hits->hits;
        $items = $response;
        //return $response;

        foreach ($items as $t) {
            $texts[] = $t->_source->data->seo;
        }
        if (isset($texts)) {
            foreach ($texts as $text) {

                if (isset($text->thumbnail)) {

                    $var [] = [
                        'title' => $text->title,
                        'description' => $text->description,
                        'uri' => $text->url,
                        'thumbnail' => $text->thumbnail,
                    ];
                } else {
                    $var [] = [
                        'title' => $text->title,
                        'description' => $text->description,
                        'uri' => $text->url,
                        'thumbnail' => 'images/test.jpg',

                    ];
                }
            }
            //return $var;
            return view('Front.result',compact('item'))->with(compact('var'));
        } else {
            return ("نتیجه ای یافت نشد!");
        }

    }
}
