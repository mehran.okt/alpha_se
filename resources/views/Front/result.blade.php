<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/front/index/bootstrap4.1.3.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/front/index/main.css">
    <script src="/js/front/jquery-slim-3.3.1.js"></script>
    <script src="/js/front/bootstrap.min.js"></script>
    <script src="/js/front/popper.min.js"></script>
    <script rel="stylesheet" href="/js/front/main.js"></script>
    <title>جستجو هوشمند آگهی</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        خودرو و سایر وسایل نقلیه
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">


                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link headlines" href="#">خودرو</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">خودرو ایرانی</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">محصولات ایران خودرو</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">محصولات ایران خودرو</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">محصولات ایران خودرو</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.col-md-4  -->
                                <div class="col-md-4">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link headlines" href="#">لوازم جانبی خودرو</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">لوازم ایران خودرو</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">لوازم چری</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">لوازم سایپا</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">لوازم جک</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.col-md-4  -->
                                <div class="col-md-4">
                                    <a href="">
                                    </a>

                                </div>
                                <!-- /.col-md-4  -->
                            </div>
                        </div>
                        <!--  /.container  -->
                    </div>
                </li>
            </ul>

        </div>
    </div>
</nav>


<div class="container" style="margin-top: 40px">
    <div class="row">
        <form class=" col-md-8 col-sm-12 col-xs-12 offset-md-2 input-group" action="{{route('search')}}" method="get">
            <input name="item" value="{{$item }}" autocomplete="off" id="pac-input" class="controls" type="text" placeholder="آگهی مورد نظر" autofocus >
            <button type="submit" class="searchSubmit"><i class="fa fa-2x fa-search" aria-hidden="true"></i>
            </button>
        </form>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <div class="filter">
                <h3>فیلتر کردن نتایج</h3>
                <div class="filterItems">
                    <div class="form-group col-sm-12">

                        <select name="" id="" class="form-control">
                            <option value="">بی رنگ</option>
                            <option value="">با رنگ</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <select name="" id="" class="form-control">
                            <option value="">بی رنگ</option>
                            <option value="">با رنگ</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <select name="" id="" class="form-control">
                            <option value="">بی رنگ</option>
                            <option value="">با رنگ</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-9">
            <div id="results">
                <div class="items">
                    @foreach($var as $item)
                    <div class="item">
                        <div class="row">
                    <div class="col-3 col-sm-2 ">
                        <img src=
                             @if(isset($item['thumbnail']))
                             {{$item['thumbnail']}}
                             @else
                                     "images/test.jpg"
                             @endif
                             alt="">
                    </div>

                    <div class="col-6 col-sm-8 ">
                            <div class="content">
                                <div class="title">
                                    {{$item['title']}}
                                </div>
                                <div class="time">دقایقی پیش</div>
                                <div class="price"><b>85,000,000 </b><span>تومان</span></div>
                            </div>
                    </div>
                            <div class="col-1 col-sm-1 offset-lg-1  source">
                                <a href="{{$item['uri']}}">
                                        <button class="btn btn-outline-danger">
                                            <i class="fa fa-2x fa-eye"></i>
                                        </button>
                                </a>
                            </div>
                        </div>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
</div>
<footer class="footer d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <a href="#">قوانین</a>
                <a href="#">درباره ما</a>
                <a href="#">تماس با ما</a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
